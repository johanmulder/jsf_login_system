package nl.localhost.jsftest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Class: SecurityHandler
 * Created by: johan
 * Created at: 2014-05-13 20:23.
 */
@SessionScoped
public class SecurityHandler implements PhaseListener
{
	@Inject
	private LoginSession loginSession;
	private static final Logger LOGGER = LoggerFactory.getLogger(SecurityHandler.class);

	/**
	 * <p>Handle a notification that the processing for a particular
	 * phase has just been completed.</p>
	 *
	 * @param event
	 */
	@Override
	public void afterPhase(PhaseEvent event)
	{
	}

	/**
	 * <p>Handle a notification that the processing for a particular
	 * phase of the request processing lifecycle is about to begin.</p>
	 *
	 * @param event
	 */
	@Override
	public void beforePhase(PhaseEvent event)
	{
		LOGGER.info("Login session: {}", loginSession);
		ExternalContext ec = event.getFacesContext().getExternalContext();
		if (isLoggedIn(event.getFacesContext()))
		{
			return;
		}

		LOGGER.info("Redirecting to login page, because the user was not logged in");
		redirectToLoginPage(ec);
	}

	/**
	 * Redirect to the login page.
	 * @param ec    The external context
	 */
	private void redirectToLoginPage(ExternalContext ec)
	{
		try
		{
			HttpServletRequest request = (HttpServletRequest) ec.getRequest();
			loginSession.setRedirectUrl(request.getRequestURI());
			LOGGER.info("Redirect url set to {}", loginSession.getRedirectUrl());
			ec.redirect(ec.getRequestContextPath() + "/login.xhtml");
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}
	}

	/**
	 * Determine if a user is logged in.
	 * @param context    The faces context
	 * @return True if logged in, false otherwise.
	 */
	private boolean isLoggedIn(FacesContext context)
	{
		FacesContext.getCurrentInstance().getExternalContext();
		return loginSession.isLoggedIn() || context.getExternalContext().getRequestServletPath().startsWith("/login");
	}

	/**
	 * <p>Return the identifier of the request processing phase during
	 * which this listener is interested in processing {@link javax.faces.event.PhaseEvent}
	 * events.  Legal values are the singleton instances defined by the
	 * {@link javax.faces.event.PhaseId} class, including <code>PhaseId.ANY_PHASE</code>
	 * to indicate an interest in being notified for all standard phases.</p>
	 */
	@Override
	public PhaseId getPhaseId()
	{
		return PhaseId.RESTORE_VIEW;
	}
}
