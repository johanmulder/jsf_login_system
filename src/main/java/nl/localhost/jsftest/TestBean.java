package nl.localhost.jsftest;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.io.IOException;

/**
 * Class: TestBean
 * Created by: johan
 * Created at: 2014-05-13 20:03.
 */
@ManagedBean(name = "testBean")
@SessionScoped
public class TestBean
{
	private String test = "test";

	public String getTest()
	{
		return test;
	}
}
