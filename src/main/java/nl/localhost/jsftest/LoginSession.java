package nl.localhost.jsftest;

import javax.enterprise.context.SessionScoped;
import java.io.Serializable;

/**
 * Class: LoginSession
 * Created by: johan
 * Created at: 2014-05-13 20:27.
 */
@SessionScoped
public class LoginSession implements Serializable
{
	private String username;
	private boolean loggedIn;
	private String redirectUrl;

	public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	public boolean isLoggedIn()
	{
		return loggedIn;
	}

	public void setLoggedIn(boolean loggedIn)
	{
		this.loggedIn = loggedIn;
	}

	public String getRedirectUrl()
	{
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl)
	{
		this.redirectUrl = redirectUrl;
	}
}
