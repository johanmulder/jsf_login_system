package nl.localhost.jsftest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.io.IOException;

/**
 * Class: LoginBean
 * Created by: johan
 * Created at: 2014-05-13 20:56.
 */
@ManagedBean
public class LoginBean
{
	private static final Logger LOGGER = LoggerFactory.getLogger(LoginBean.class);
	private String username;
	private String password;
	@Inject
	private LoginSession loginSession;

	public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public void login()
	{
		if ("foo".equals(username) && "bar".equals(password))
		{
			loginSession.setLoggedIn(true);
			ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
			try
			{
				String redirectUrl = loginSession.getRedirectUrl();
				loginSession.setRedirectUrl(null);
				if (redirectUrl == null)
				{
					redirectUrl = ec.getApplicationContextPath();
				}

				LOGGER.info("Redirecting to {}", redirectUrl);
				LOGGER.info("login session is {}", loginSession);
				ec.redirect(redirectUrl);
				return;
			}
			catch (IOException e)
			{
				throw new RuntimeException(e);
			}
		}
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Login failed",
				"Username or password incorrect");
		FacesContext.getCurrentInstance().addMessage(null, message);
	}
}
